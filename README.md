VX 805 Out of Memory Error

When your Verifone VX805 contains the error message ‘Out of Memory”, it may be because the VX 805 PIN pads loaded with XPI 8.42b are lacking two parameters that are required to ensure dependable performance.
In order to correct this error message, you can follow the procedure indicated below to set these parameters from the PIN pad’s system menu manually.

1.       Click “F2” and “F4” to enter system mode.
The password is “1 ALPHA ALPHA 6 6 8 3 1 Enter”
Alpha is a black key placed directly on top of the ‘2’ Key. Enter is the Green key
 
2.       Click “2” for Edit Parameters
3.       Click ENTER and choose “1″ for Group ID
The password is “1 ALPHA ALPHA 6 6 8 3 1”
 
4.       Click ENTER to choose CONFIG.SYS
5.       Click “1” for New
6.       For the parameter name, enter *DHEAP
For the alpha characters, click the associated number key followed by ALPHA until the correct letter appears. * is located below the 7 key.
 
7.       Click ENTER to verify the new parameter.
8.       For the value, enter 0.
9.       Click ENTER to verify the new value.
10.   Click 1 for New again.
11.   For the parameter name, enter #CZE. For the alpha characters, click the associated number key followed by ALPHA until the correct letter shows.
# is located below the 9 key.
12.   Click ENTER to verify the new parameter.
13.   For the value, enter 1.
14.   Click ENTER to verify the new value.
15.   Click CANCEL.
16.   Click 1 for “Restart”.
